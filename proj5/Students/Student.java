package Students;

import Persons.*;

public class Student extends Person{

   String group;

   public Student(String n, String s, int a, String p, String g){
 
      super(n, s, a, p);
      this.group = g; 
   }

   public void showStudent(){
      
      System.out.println("");
      super.showPerson();
      System.out.print(" Group: " + group);
   }
}
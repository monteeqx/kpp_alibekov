import Students.*;
import Persons.*;
import Teachers.*;

class myClass{

 public static void main (String args[]){

   Person p = new Person("Stepan", "Ivanov", 68, "0995235123");
   p.showPerson();

   Teacher t = new Teacher("Ivan", "Fedorovich", 47, "0683211582", "Mathematician", "algebra, geometriya");
   t.showTeacher();

   Student s = new Student("Fedor", "Stepanovich", 24, "06963511322", "RA-151");
   s.showStudent();
 }

}
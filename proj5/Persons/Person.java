package Persons;

public class Person{

 private String name;
 private String surname;
 private int age;
 private String phone;

 public Person(String n, String s, int a, String p){
	  
   this.name = n;
   this.surname = s;
   this.age = a;
   this.phone = p;
 }

 public void showPerson(){
 
   System.out.print("Name: " + name + " Surname: "+ surname + " Age: " + age + " Phone: " + phone);
 }
 
}